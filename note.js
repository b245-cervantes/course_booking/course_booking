//[Bcrypt]
  //npm install bcrypt / npm i bcrypt

  //In our application we willl be using this package to demonstrrate how to encrypt data when auser
  // register to our website

  // The "bcrypt" package is one of the many packages that we can use tp encrypt information but is not 
  //commonly recommended of how simple the algo is.

  //There are other more advanced encrpyion packages that can be used.

  //syntax for hashing the password
     // syntax:
    //  bcrypt.hashSync(password, saltRounds)
   //saltRounds is the value provided as the number of "salt" round that the bcrypt algorithm will
   //run in order to encryp the password.

   //[JWT - jsonwebtoken]
     //JSON web tokens is an industry standard for sending information between our application in a 
     //secure manner

     //the jsonwebtoken package will allow us to gain access to methods that will help us
     //create JSON web token

     // first we have to get userId and the courseId
        // decode the token to extract/unpack the payload
        // const userData = auth.decode(req.headers.authorization);
        // //get the courseId by targetting the params
        // const courseId = req.params.courseId;
        //  console.log(userData);
        // //2 things that we need to do in this controller
        //    //first, to push the courseId in the enrollments property of the user
        //    //second, to push the userId in the enrolless property of the course.

        //  let isUserIdupated = await Users.findById(userData._id)
        //    .then(result => {
        //     console.log(result)
        //       if(result === null){
        //         return false
        //       } else {
        //         result.enrollments.push({courseId: courseId})
        //         return result.save()
        //         .then(save =>{
        //             return true
        //         })
        //         .catch(error => {
        //             console.log(error)
        //             return false
        //         })
        //       }
        //    })
              
        //    let isCourseUpdated = await Course.findById(courseId)
        //    .then(result => {
        //     console.log(result)
        //        if(result === null){
        //         return false
        //        } else {
        //         result.enrolles.push({userId: userData._id})
        //         return result.save()
        //         .then(save => {
        //             return true
        //         })
        //         .catch(error => {
        //             console.log(error)
        //             return false
        //         })
        //        }
        //    })
        //    console.log(isCourseUpdated)
        //       console.log(isUserIdupated)
        //    if(isCourseUpdated && isUserIdupated){
        //     return res.send("The Course is now enrolled!")
        //    }else {
        //     return res.send("There was an error during the enrollment. Please try again")
        //    }