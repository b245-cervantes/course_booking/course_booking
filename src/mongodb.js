const express = require('express')
const mongoose = require('mongoose')
const mongodb = express.mongodb;


mongoose.set('strictQuery',true);
//[mongodb connection]
mongoose.connect("mongodb+srv://admin:admin@batch245-cervantes.ttw2wvw.mongodb.net/batch245_Course_API_Cervantes?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection;

//for error handling
db.on("error", console.error.bind(console, "error")
)
//For validation of the connection
db.once("open", () => 
  console.log("We are connected to cloud server!!")
)
module.exports = mongodb;
