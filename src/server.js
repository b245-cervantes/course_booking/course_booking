const express = require('express')
//Objest model manger(ODM)

//by default our backend CORS setting will prevent any application outside
// our express JS app to process the request. USing the cors package, it will
// allow us to manipulate this and control what application may use our app

// Allows our backend application to be available to our frontend application
//ALlows us to control the app cross origin resourse sharing
const cors = require('cors')
const app = express()
const port = 2020
const mongodb = require('./mongodb')
const userRoute = require('../Routes/userRoutes')
const courseRoutes = require('../Routes/courseRoutes')



//middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

app.use("/user", userRoute)
app.use("/course", courseRoutes)

app.listen(port, ()=>{
    console.log(`The Server is running at port ${port}`)
})