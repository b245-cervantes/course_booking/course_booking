const express = require('express');
const router = express.Router();
const courseController = require('../Controllers/courseController');
const auth = require('../auth');

//Route for creating a course

router.post('/',auth.verify, courseController.addCourse);


//Route for retrieving all courses
router.get('/all', auth.verify, courseController.allCourses);

//route for retrieving all active courses
router.get('/allActive', courseController.allActiveCourses);
router.get('/allInactive',auth.verify, courseController.allInactiveCourses)


router.put('/update/:courseId', auth.verify, courseController.updateCourse)

router.patch('/:courseId/archive', auth.verify, courseController.archiveCourse)

//routes with params
router.get('/:courseId', courseController.courseDetails)

module.exports =router;