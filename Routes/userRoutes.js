const express = require('express');
const router = express.Router();
const userController = require('../Controllers/usesrController');
const auth = require('../auth');



//[Routes]
//this is responsible for the registration of the user
router.post('/register', userController.userRegistration);
//THis route is for the user authentication
router.post('/login', userController.userAuthentication);

router.get('/details',auth.verify ,userController.getProfile);

//route for enrollment
router.post('/enroll/:courseId', auth.verify, userController.enrollCourse)

module.exports = router;
