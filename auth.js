const jwt = require('jsonwebtoken');
//User defined string data that will be used to create JSON web tokens
//used in the algo for encrypting our data which makes it difficult to decode
// the information without defined secret keyword.

const secret = "CourseBookingAPI";

//SECTION JSON web token
  //JSON webtoken or jwt is a way of securely passing the server to the frontend or the other
  //parts of the server

  //Information is kept secure through the use of the secret code
  //only the system will know the secret code that can be decode the encrypted information

  //Token creation

  /**
   * Analogy:
   *   pack the gift/information and provide the secret code for the key 
   *
   */
   //the argument that will be passed to our parameter(user) wil be the document/information of our user.
  module.exports.createAccestoken = (user) =>{
    //payload,
    //will contain the data that will be passed to other parts of our API
     const data = {
        _id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
     }
     //.sign() from jwt package will generate a JSON web token
     //Syntax:
        //jwt.sign(payload,secretcode, options)
     return jwt.sign(data, secret,{})
  }

  //token verification

  /**
   *  analoguy
   *   -Recieved the gift and open the lock to verify if the 
   * sender is legitimate and the gift wast not tampered
   * 
   * */

  //middleware function have access wuth request object and respond object
  //and the next function indicates that we may proceed with the next step.

  module.exports.verify = (req,res,next) =>{
    //the token is retrieved from the request headers
    //this can be provided in postman under
      //Authorization > Bearer Token

      let token = req.headers.authorization;

      //Token received and is not undefiend
      if(typeof token !== "undefined"){
           //Retrieves only token and removes the "Bearer " prefix
           token = token.slice(7, token.length)


           //validate the token using the "verify" method decrypting the token using the secret coe.
           //jwt.verify(token, secret or privateKEy, [option/callbackFunction])
           return jwt.verify(token, secret, (error, data) => {
            //if JWT is not valid 
            if(error){
                return res.send({auth: "Failed!"})
            } else {
                //The verify method will be used as middleware in
                // the route to verify the token before proceeding to the 
                //function that invokes the controller function.
                next();
            }
           })
           //Token does not exists
      }else {
        return res.send({auth: "Failed!."})
      }
  }


  //token decryption
  /**
   *  analogy:
   *   -Open the gift and get the content.
   * 
   */

    module.exports.decode = (token) => {
        //Token received is not undefined 
        if(typeof token !== "undifined") {
           token = token.slice(7, token.length);
           
           return jwt.verify(token,secret, (error, data)=>{
              if(error){
                return null
              } else {
                // The "decode" method is used to obtain information form the JWT.
                // jwt.decode(token, [options])
                //return an object with access to the "payload" property, which contains user information stored when
                //the token garanted
                return jwt.decode(token, {complete:true}).payload;
              }
           })

        }
        //Token does not exist(undeifiend)
        else{
            return null
        }
    }