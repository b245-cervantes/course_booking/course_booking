const mongoose = require("mongoose")

const coursesSchema = new mongoose.Schema({
    name: {
        type:String,
        required: [true, "Name of the course is required"]
    },
    description:{
        type:String,
        required: [true, "Description of the course is required"]
    },
    price: {
        type:Number,
        required: [true, "Price of the course is required!"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        //the new date() expression instantiates a new "date" that stores current date 
        // and time whenever a course is created
        default: new Date()
    },
    enrollees: [
        {
            userId:{
                type:String,
                required:[true, "UserId is required!"]
            },
            enrolledOn: {
                type:Date,
                default: new Date()
            }
        }
    ]
})

module.exports = mongoose.model("Course", coursesSchema);