const Course = require('../Models/coursesSchema');
const auth = require('../auth')
// Create a new course
/*
    Steps:
    1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
    2. Save the new User to the database
*/

module.exports.addCourse = (req,res) => {
    let input = req.body
    const courseTobeAdded = auth.decode(req.headers.authorization)

    if(courseTobeAdded.isAdmin !== true){
        return res.send("This Features is not capable for you!! its only for admins!")
    }else {
        let newCourse = {
            name:input.name,
            description:input.description,
            price: input.price
        }

            Course.insertMany([newCourse])
        res.send("Successfully added")
            
    }
     
} 

// Create a controller wherein it will retrieved all the courses. (active/inactive courses)
module.exports.allCourses = (req,res) => {
    const userData = auth.decode(req.headers.authorization)

    if(!userData.isAdmin){
        return res.send("You dont have access to this route")
    }else {
        Course.find({})
        .then(result => {
            return res.send(result)
        })
        .catch(error =>{
            console.log(error)
            return res.send(false)
        })
    }
}

//Create a controller wherein it will retrieved all the courses that are active

module.exports.allActiveCourses = (req,res) => {
    Course.find({isActive: true})
    .then(result => {
        return res.send(result)
    })
    .catch(error => {
        console.log(error)
        return res.send(false)
    })
}

//Create a controller wherein tit will retrieved all the coursees are inactive

module.exports.allInactiveCourses = (req,res) =>{
   
    const userData = auth.decode(req.headers.authorization)
    if(!userData.isAdmin){
        return res.send("you cannot accesss this route")
    } else {
        Course.find({isActive: false})
        .then(result => {
            return res.send(result)
        })
        .catch(error => {
            console.log(error)
            return res.send(false)
        })
    }

   
}

//This controller will get the details of specific course

module.exports.courseDetails = (req,res) => {
    //to get the params from the url
    let courseId = req.params.courseId;

    Course.findById(courseId)
    .then(result => {
        return res.send(result)
    })
    .catch(error => {
        console.log(error)
        return res.send(false)
    })
}

//This controller is for updating specific course
/**
 *  Business logic:
 *     1. We are goin to edit/update the course, that is stored in the params.
 *     
 * 
 */

module.exports.updateCourse = (req,res) =>{
    const userData = auth.decode(req.headers.authorization)
    const courseId = req.params.courseId;
    const input = req.body;

    if(!userData.isAdmin){
        return res.send("You don't have access in this page!")
    } else {
        Course.findById(courseId)
        .then(result => {
            if(result === null){
                return res.send("CourseId is invalid, please try again!")
            } else {
                let updatedCourse = {
                    name: input.name,
                    description:input.description,
                    price: input.price
                }
                Course.findByIdAndUpdate(courseId, updatedCourse, {new:true})
                .then(result => {
                    return res.send(result)
                })
                .catch(error => {
                    console.log(error)
                    return res.send(false)
                })
            }
        })
        .catch(error => {
            console.log(error)
            return res.send("INVALID INPUT!!")
        })
    }
}


//this controller is for archiving courses

module.exports.archiveCourse = (req,res) =>{
    const userData = auth.decode(req.headers.authorization)
    const input = req.body;
    const courseId = req.params.courseId;

    if(!userData.isAdmin){
        return res.send("You dont have access in this route!!")
    } else {
         Course.findById(courseId)
         .then(result => {
          if(result === null){
            return res.send("invalid course ID")
          } else{
            let deleteCourse = {
                isActive:input.isActive
            }
            Course.findByIdAndUpdate(courseId, deleteCourse, {new:true})
            .then(result => {
                return res.send(result)
            })
            .catch(error => {
                console.log(error)
                return res.send(false)
            })
          }
         })
         .catch(error => {
            console.log(error)
            return res.send("Invalid input!")
         })
    }
}