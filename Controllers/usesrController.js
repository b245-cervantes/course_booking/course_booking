const mongoose = require('mongoose');
const Users = require('../Models/userSchema');
const bcrypt = require('bcrypt');
const auth = require('../auth')
const Course = require('../Models/coursesSchema');


//Controllers


//This controller will create or register a user on our db/database

module.exports.userRegistration = (req,res) => {
const input = req.body;

Users.findOne({email:input.email})
.then(result =>{
    if(result !==null){
        return res.send("The email is already taken!!")
    } else {
        let newUser = new Users ({
            firstName:input.firstName,
            lastName:input.lastName,
            email:input.email,
            password:bcrypt.hashSync(input.password, 10),
            mobileNo:input.mobileNo
        })
        newUser.save()
        .then(save =>{
            return res.send("You are now registered to our website!")
        })
        .catch(error => {
            return res.send(error)
        })
    }
})
.catch(error => {
    return res.send(error)
})
}
//User authentication
module.exports.userAuthentication = (req, res) =>{
let input = req.body;
//possible scenarios in loggin in
//1. email is not yet registered
//2. email is registered but the password is wrong

Users.findOne({email:input.email})
.then(result => {
if(result === null){
    return res.send("Email is not yet registered. Register first b efore loggin in!")
} else {
    //we have to verify if the password is correct
    // The "compareSync" method is used to compare a non encrypted password to the encrypted password.
    //it returns boolean value, if match true value will return otherwise false.
    const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

    if(isPasswordCorrect){
        return res.send({auth:auth.createAccestoken(result)})
    }else {
        return res.send("Password is incorrect!")
    }
} 
})
.catch(error => {
res.send(error);
})
}

//Get profile controller

    module.exports.getProfile = (req, res) =>{
            // let getProfileUser = req.body;

            const userData = auth.decode(req.headers.authorization);

            console.log(userData);
            Users.findById(userData._id)
            .then(result => {
                result.password="confidential"
                return res.send(result)
            })
            .catch(error => {
                return res.send(error)
            
        })
    }

    //Controller for user enrollment:
    //1. We can get the id of the user by decoding the jwt
    //2. we can get the courseId by using the request params

    module.exports.enrollCourse = async(req,res) => {
        const userData = auth.decode(req.headers.authorization);
        const courseId = req.params.courseId;
        
            if(userData.isAdmin){
                return res.send("Only The Users can access this feature!!")
            }else if(!userData.isAdmin){

                let isCourseIdUpdated = await Course.findById(courseId)
        .then(result =>{
            if(result === undefined){
                 return res.send("INVALID course ID! please try again!")
            } else {
                result.enrollees.push({userId: userData._id})
              return result.save()
                .then(save => {
                    return true
                })
                .catch(error => {
                    console.log(error)
                    return res.send(false)
                })
            }
        })
        .catch(error => {
            console.log(error)
            return res.send("INVALID INPUT!!")
        })

        let isUserIdUpdated = await Users.findById(userData._id)
        .then(result => {
            if(result === undefined){
                return res.send("Invalid user ID! please try again!")
            } else {
                result.enrollments.push({courseId: courseId})
                return result.save()
                .then(save => {
                    return true
                })
                .catch(error => {
                    console.log(error)
                    return res.send(false)
                })
            }
        })
        .catch(error =>{
            console.log(error)
            res.send("INVALID INPUT!!")
        })

        if(isCourseIdUpdated && isUserIdUpdated){
            return res.send("The course is now enrolled!")
        } else {
            return res.send("There was an error during the enrollment. Please try again")
        }
            } else {
                res.send("ERROR! ERROR!")
            }
    }



